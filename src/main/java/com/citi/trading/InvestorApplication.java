package com.citi.trading;

import java.util.HashMap;
import java.util.Map;

/**
 * Test application for the {@link Investor} system.
 * 
 * @author Will Provost
 */
public class InvestorApplication {

	/**
	 * Create three investors, and for each one request a trade.
	 * Sleep a while to allow for messaging with the stock market,
	 * and then show the updated portfolio and cash balance.
	 */
	public static void main(String[] args) {
		
		try {
			Market market = new Market();
			
			Investor investor1 = new Investor(40000, market);
			investor1.buy("KHC", 100, 100);
			try {
				Thread.sleep(1500);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			System.out.println("Investor 1 buys KHC:");
			System.out.println(investor1.getPortfolio());
			System.out.println(investor1.getCash());
			System.out.println();
			
			Map<String,Integer> starter2 = new HashMap<>();
			starter2.put("MSFT", 10000);
			Investor investor2 = new Investor(starter2, 0, market);
			investor2.sell("MSFT", 1000, 100);
			try {
				Thread.sleep(1500);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			System.out.println("Investor 2 sells some MSFT:");
			System.out.println(investor2.getPortfolio());
			System.out.println(investor2.getCash());
			System.out.println();
	
			Map<String,Integer> starter3 = new HashMap<>();
			starter3.put("MSFT", 10000);
			Investor investor3 = new Investor(starter3, 0, market);
			investor3.sell("MSFT", 10000, 100);
			try {
				Thread.sleep(1500);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			System.out.println("Investor 3 sells all MSFT:");
			System.out.println(investor3.getPortfolio());
			System.out.println(investor3.getCash());
			System.out.println();
		} finally {
			System.exit(0);
		}
	}
}
